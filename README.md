# google-homepage

Created with HTML and CSS as part of The Odin Project [Curriculum](https://www.theodinproject.com/courses/web-development-101/). 

# Final Thoughts 

Completing this project allowed me to review HTML and CSS fundamentals and practice styling a webpage with elements such as searchbars, buttons and navbars that are common throughout the internet.